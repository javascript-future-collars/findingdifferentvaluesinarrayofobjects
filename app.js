const employees = [
  {
    id: 1,
    firstName: "Adrian",
    lastName: "Tumski",
    age: 44,
    yearsToRetirement: 26,
    born: 1977,
  },
  {
    id: 2,
    firstName: "Juliana",
    lastName: "Tuwim",
    age: 22,
    yearsToRetirement: 43,
    born: 1982,
  },
  {
    id: 3,
    firstName: "Drzewiec",
    lastName: "Ślimaczy",
    age: 88,
    yearsToRetirement: 0,
    born: 1933,
  },
];
let myChoices = {
  adrianTumskiId: employees[0].id,
  thirdEmployeeId: employees[2].id,
  firstEmployeeName: employees[0].firstName,
  secondEmployeeFullName: `${employees[1].firstName} ${employees[1].lastName}`,
  secondEmployeeYearOfBirth: employees[1].born,
  firstEmployeeAge: employees[0].age,
  thirdEmployeeYearsToRetirement: employees[2].yearsToRetirement,
  adrianTumskiRetirementAge: employees[0].age + employees[0].yearsToRetirement,
};
